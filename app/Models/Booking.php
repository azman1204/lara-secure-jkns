<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    use HasFactory;
    protected $fillable = ['book_dt', 'descr', 'is_approved', 'created_by'];
    // protected $fillable = ['book_dt', 'descr'];
}
