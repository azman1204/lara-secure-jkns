<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Post;
use Illuminate\Auth\Access\HandlesAuthorization;

class PostPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    
    // utk super user, dapa semua permission
    public function before(User $user, $ability) {
        return $user->role == 'super';
    }

    public function create(User $user, Post $post) {
        //dd($user->role);
        if ($user->role == 'author') {
            return true;
        } else {
            return false;
        }
    }

    // author yg create post, juga boleh edit
    public function edit(User $user, Post $post) {
        if ($user->id == $post->created_by)
            return true;
        else
            return false;
    }

    // siapa yg boleh view post ? author OR adm
    public function view(User $user, Post $post) {
        if ($user->role == 'author' || $user->role == 'adm')
            return true;
        else
            return false;
    }

    // hapus data
    public function delete(User $user, Post $post) {
        return $user->role == 'adm';
    }
}
