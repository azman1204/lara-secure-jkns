<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use App\Models\Booking;
use App\Models\Post;
use App\Policies\PostPolicy;
use Laravel\Passport\Passport;


class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Models\Post' => 'App\Policies\PostPolicy',
        //Post::class => PostPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        if (! $this->app->routesAreCached()) {
            Passport::routes();
        }

        Gate::define('edit', function($user, Booking $booking) {
            if ($user->id == $booking->created_by) {
                // org yg login adalah org create booking
                return true; // boleh edit
            } else {
                return false; // tak boleh edit
            }
        });

        Gate::define('usr', function($user) {
            if ($user->role == 'usr') {
                return true;
            } else {
                return false;
            }
        });
    }
}
