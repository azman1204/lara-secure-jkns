<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\Models\Log;
use Illuminate\Support\Facades\Auth;

class LogRequest
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        // code kita disini
        $log = new Log();
        $log->url = $request->getRequestUri();
        $log->referer = url()->previous();
        $data = $request->all();
        if ($request->has('password')) {
            unset($data['password']);
        }
        $log->data = json_encode($data);
        $log->ip = $request->ip();
        $log->agent = $request->header('User-Agent');
        $log->method = $request->method();

        if(Auth::check()) {
            //logged-in user
            $log->user_id = $request->user()->id;
            $log->user_id = $request->user()->id;
        }
        $log->save(); // create maklumat request

        $response = $next($request);
        $log->status_code = $response->getStatusCode();
        $log->response_at = date('Y-m-d H:i:s');
        $log->save(); // udpate maklumat response
        return $response;
    }
}
