<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Article;

class ArticleController extends Controller
{
    // globally shared article
    public function all() {
        \Log::info("Test");
        $tot = 10/0;
        $articles = Article::paginate(5);
        return view('article.all', compact('articles'));
    }

    public function create2() {
        return view('article.form');
    }

    // user x ada direct access
    public function showImage($id) {
        $article = Article::find($id);
        $content = \Storage::get('upload/'.$article->attachment);
        // beritahu browser jenis content yg dihantar. default text/html
        //return response($content)->header('Content-Type', 'image/png');
        $path = \Storage::path('upload/'.$article->attachment);
        return response($content)->header('Content-Type', 'application/pdf');//\Storage::mimetype($path)
    }

    public function save(Request $req) {
        $rules = [
            'content' => 'required',
            // 'attachment' => 'required|image'
            // 1024 = 1MB
            'attachment' => 'required|mimeTypes:application/pdf|max:1024'
        ];
        $req->validate($rules);
        // validation ok
        $article = new Article();
        $article->content = $req->content;
        $article->created_by = \Auth::user()->id;
        $img = $req->file('attachment');
        $article->attachment = $img->getClientOriginalName();
        // default : storage/app
        // storage/app/public/images
        // $req->attachment->storeAs('public/images', $img->getClientOriginalName());
        $req->attachment->storeAs('upload', $img->getClientOriginalName());
        $article->save();
        echo "Article created successfully";
    }

    public function create() {
        $article = new Article();
        $article->content = "Hello World";
        $article->created_by = \Auth::user()->id;
        $article->save();
        echo "Article created successfully";
    }

    public function edit(Article $article) {
        $user = \Auth::user();
        if (! $user->can('edit articles')) {
            abort(403, 'Anda tiada permission');
        }

        $article->content = 'Edited';
        $article->save();
        echo "Article edited successfully";
    }

    public function publish(Article $article) {

    }

    // show all articles
    public function list() {
        $articles = Article::all();
        return view('article.list', compact('articles'));
    }
}
