<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;

class PostController extends Controller {
    public function create(Request $request) {
        //auth()->user()
        // \Auth::user()
        $post = new Post();
        if($request->user()->cannot('create', $post)) {
            abort(403);
        }

        //return "Yes, you can create a new post";
        return view('post.form', compact('post'));
    }

    // insert & update
    public function save(Request $req) {
        $post = new Post();
        $user = auth()->user();
        // authorization
        if ($user->cannot('create', $post)) {
            abort(403);
        }

        $id = $req->id;
        if (! empty($id)) {
            // update
            $post = Post::find($id);
        }

        $post->created_by = $user->id;
        $post->content = $req->content;
        $post->save();
        echo "Post saved";
    }

    // show all post
    public function list() {
        //dd(auth()->user()->role);
        // authorization
        $user = auth()->user();
        if ($user->cannot('view', new Post())) {
            abort(403, 'Tiada Permission');
        }

        $posts = Post::all();
        return view('post.list', compact('posts'));
    }

    public function delete($id) {
        $user = auth()->user();
        // if ($user->cannot('delete', new Post())) {
        //     abort(403);
        // }

        Post::find($id)->delete();
        return redirect()->back();
    }
}
