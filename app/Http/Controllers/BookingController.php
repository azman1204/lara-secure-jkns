<?php
/**
 * demo form yang boleh dimanipulasi datanya 
 * - create()
 */
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests\BookingRequest;
use App\Models\Booking;
use Illuminate\Support\Facades\Gate;

class BookingController extends Controller {
    // show booking form
    public function create() {
        return view('booking.form');
    }

    // guna $fillable
    // public function save(Request $req) {
    public function save(BookingRequest $req) {
        // boleh dimanipulate. solution #1, $fillable
        //Booking::create($req->all());

        // solution #2, only(), except()
        //Booking::create($req->only(['book_dt', 'descr']));
        //Booking::create($req->except(['is_approved']));

        // solution #3 - validation
        // $data = $req->validate([
        //     'book_dt' => 'required',
        //     'descr' => 'required'
        // ]);

        //print_r($req->all());
        //dd($data);
        // Booking::create($data);

        // solution # 4 - validation dalam  Request class
        // replace <script> guna regular expression
        $data = $req->validated();
        //dd($data);
        $data['descr'] = preg_replace("#<script(.*?)>(.*?)</script>#is", '', $data['descr']);
        // remove semua html tags
        //$data['descr'] = strip_tags($data['descr']);
        // Booking::create($req->validated());
        $data['created_by'] = \Auth::user()->id;
        Booking::create($data);
    }

    // admin update status. teknik ini x perlu $fillable
    public function update($id) {
        $booking = Booking::find($id);
        $booking->is_approved = 'Y';
        $booking->save();
    }

    public function search(Request $req) {
        $id = $req->id; // user input
        // input 1 OR 1=1
        // teknik ini bahaya, terdedah kepada SQL injection
        //$rs =\DB::select(\DB::raw("select * from bookings where id=$id"));

        // solution - data binding, data cleaning
        // $id = (int)$id;
        // $rs =\DB::select(\DB::raw("select * from bookings where id=:id"),
        //  ['id' => $id]);
        $rs = Booking::where('id', $id)->get();
        return view('booking.search', compact('rs'));
    }

    public function searchForm() {
        return view('booking.search');
    }

    // form submission guna js/AJAX
    public function form2() {
        return view('booking.form2');
    }

    public function edit($id) {
        $booking = Booking::find($id);
        
        // authorize
        if (! Gate::allows('edit', $booking)) {
            abort(403);
        }
        
        //dd($booking);
        return view('booking.edit', compact('booking'));
    }

    // update booking
    public function store(Request $req) {
        $booking = Booking::find($req->id);
        // authorize
        if (! Gate::allows('edit', $booking)) {
            abort(403);
        }

        $booking->book_dt = $req->book_dt;
        $booking->descr = $req->descr;
        $booking->save();
        echo "Booking updated";
    }
}
