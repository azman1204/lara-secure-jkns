<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Models\user;

class PermissionController extends Controller
{
    public function permission1() {
        // create role = writer
        //$role = Role::create(['name' => 'writer']);
        $role = Role::create(['name' => 'publisher']);
        // create permission = 'edit aticles'
        //$permission = Permission::create(['name' => 'edit articles']);
        $permission = Permission::create(['name' => 'publish articles']);
        $role->givePermissionTo($permission);
        //$permission->assignRole($role);
    }

    public function role1() {
        // azman = writer, john doe = publisher
        $user = User::where('email', 'azman1204@yahoo.com')->first();
        $user->assignRole('writer');

        $user = User::where('email', 'john.doe@gmail.com')->first();
        $user->assignRole('publisher');

        echo "Role assignment completed";
    }

    public function permission2() {
        $user = User::where('email', 'azman1204@yahoo.com')->first();
        Permission::create(['name' => 'create articles']);
        $user->givePermissionTo('create articles');
        echo "Permission created";
    }
}
