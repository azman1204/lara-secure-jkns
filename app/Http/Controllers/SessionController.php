<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SessionController extends Controller
{
    public function index() {
        // set session
        session(['nama' => 'Azman', 'alamat' => 'bangi']);
        return redirect('/session/page2');
    }

    public function page2() {
        // display session
        return view('session.page2');
    }

    public function flush() {
        // clearkan all session properly
        session()->forget('nama');
        //session()->flush();
        return redirect('/session/page2');
    }
}
