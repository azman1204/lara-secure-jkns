<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Post;

class PostController extends Controller
{
    public function list() {
        return Post::paginate(3);
    }

    public function create(Request $req) {
        // authorization
        if (auth()->user()->cannot('create articles')) {
            return ['status' => 'No Authorization'];
        }

        $data = $req->all();
        $rules = ['content' => 'required'];
        $msg = ['msg.required' => 'kandungam wajib diisi'];
        $v = \Validator::make($data, $rules, $msg);
        if($v->passes()) {
            $post = new Post();
            $post->content = $data['content'];
            $post->created_by = \Auth::user()->id;
            $post->save();
            return ['status' => 'post created'];
        } else {
            $err = $v->errors();
            return $err;
        }
    }

    public function show($id) {

    }

    public function edit($id) {

    }

    public function update(Request $req) {
        if(auth()->user()->cannot('create articles')) {
            return ['status' => 'No authorization'];
        }

        $req->validate(['content' => 'required', 'id'=>'required']);
        // validation ok
        $post = Post::find($req->id);
        if($post) {
            $post->content = $req->content;
            $post->save();
            return response()->json(['status' => 'success']);
        } else {
            return response()->json(['status' => 'Record not found']);
        }
    }

    public function delete() {

    }
}
