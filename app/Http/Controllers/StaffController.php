<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Staff;

class StaffController extends Controller
{
    public function create() {
        $staff = new Staff();
        $staff->id = \Str::uuid();
        $staff->name = 'Abu Hassan';
        $staff->save();
        echo "New staff has been created";
    }

    public function encrypt() {
        echo encrypt('azman');
    }

    public function decrypt() {
        echo decrypt('eyJpdiI6IjBGM0VrSkhpWG1BZVEwdHE2cDRmMnc9PSIsInZhbHVlIjoibWFMVzJtTzYrdllmNmJSOHB5Nk9Kdz09IiwibWFjIjoiMWU0Yzg2MDM5OWIyNTdjNzY5Zjg1MWQ4ZWVmOWQwYjY1MzZmYzY5MTYwMWIxNTkyNGE5MDc1YWU3ZmFlNmJiMyJ9');
    }
}
