<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rules\Password;
use App\Models\User;

class UserController extends Controller
{
    // login screen
    public function login() {
        return view('user.login');
    }

    // login authentication
    public function auth(Request $req) {
        $credentials = [
            'email' => $req->email,
            'status' => 'A',
            'password' => $req->password
        ];

        if(\Auth::attempt($credentials)) {
            return redirect('/dashboard');
        } else {
            // error msg biar general
            return redirect()->back()->with('err', 'Credential error');
        }
    }

    // logout
    public function logout() {
        \Auth::logout();
        session()->flush();
        return redirect('/login');
    }

    // registration
    public function registration() {
        return view('user.registration');
    }

    // save registration
    public function register(Request $req) {
        // validation
        $rules = [
            'name' => 'required|min:3',
            'email' => 'required|email|unique:users',
            'password' => [
                'required',
                'confirmed',
                Password::min(8)->letters()->mixedCase()->numbers()->symbols()
            ]
        ];

        // validation, on fail return back to regitration page
        $req->validate($rules);

        // validation ok
        $user = new User();
        $user->email = $req->email;
        $user->name = $req->name;
        $user->password = \Hash::make($req->password); // bcrypt()
        $user->remember_token = \Hash::make(date('Y-m-d H:i:s'));
        $user->save();

        // send email validation. user.email = blade fail
        \Mail::send('user.email', compact('user'), function($m) use ($user) {
            $m->to($user->email);
            $m->subject('Registration Validation');
        });

        echo "User registered";
    }

    public function verify($token) {
        $user = User::where('remember_token', $token)->first();
        if ($user) {
            $user->status = 'A';
            $user->email_verified_at = date('Y-m-d H:i:s');
            $user->remember_token = null;
            $user->save();
            return redirect('/user/login');
        }
    }
}
