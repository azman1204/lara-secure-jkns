<?php
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BookingController;
use App\Http\Controllers\SessionController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\PermissionController;
use App\Http\Controllers\ArticleController;
use App\Http\Controllers\StaffController;
use Illuminate\Http\Request;
use TimeHunter\LaravelGoogleReCaptchaV3\Validations\GoogleReCaptchaV3ValidationRule;

// captcha
Route::view('/captcha', 'captcha');
Route::post('/verify', function(Request $req) {
    //dd(request()->all());
    $rule = [
        'g-recaptcha-response' => [new GoogleReCaptchaV3ValidationRule('signup')]
    ];
    $req->validate($rule);
    echo "ok";
});

// jana token
// dlm reality, adm ada UI utk create user dan jana token
Route::get('/gen-token', function() {
    $user = \App\Models\User::find(2);
    $user->api_token = \Str::random(60);
    $user->save();
});

// revoke sanctum token utk semua orang
Route::Get('/revoke-token', function() {
    $users = \App\Models\User::all();
    foreach ($users as $user) {
        $user->tokens()->delete();
    }
});

// staff
Route::get('/staff/create', [StaffController::class, 'create']);
Route::get('/staff/encrypt', [StaffController::class, 'encrypt']);
Route::get('/staff/decrypt', [StaffController::class, 'decrypt']);

// article
Route::middleware(['auth'])->prefix('/article')->group(function() {
    // show image
    Route::get('/show-image/{id}', [ArticleController::class, 'showImage']);
    // ada upload file
    Route::get('/create2', [ArticleController::class, 'create2']);
    Route::post('/save', [ArticleController::class, 'save']);
    Route::get('/create', [ArticleController::class, 'create'])->middleware('can:create articles');
    Route::get('/edit/{article}', [ArticleController::class, 'edit'])->middleware('can:edit articles');
    Route::get('/publish', [ArticleController::class, 'publish'])->middleware('can:publish articles');
    Route::get('/list', [ArticleController::class, 'list']);
});

// utk global access
Route::get('/article/all', [ArticleController::class, 'all']);

// spatie Role & Permission
Route::prefix('/spatie')->group(function() {
    Route::get('/permission1', [PermissionController::class, 'permission1']);
    Route::get('/role1', [PermissionController::class, 'role1']);
    // direct permission. permission yg dia dapat tanpa kaitan dgn role
    Route::get('/permission2', [PermissionController::class, 'permission2']);
});


// login manually
Route::prefix('/user')->group(function() {
    Route::get('/login', [UserController::class, 'login']);
    Route::post('/auth', [UserController::class, 'auth'])->middleware('throttle:3,1');
    Route::get('/logout', [UserController::class, 'logout']);
    Route::get('/registration', [UserController::class, 'registration']);
    Route::post('/registration', [UserController::class, 'register']);
    Route::get('/verify/{token}', [UserController::class, 'verify']);
});


// Post
Route::prefix('/post')->middleware(['auth'])->group(function() {
    Route::get('/create', [PostController::class, 'create']);
    Route::get('/list', [PostController::class, 'list']);
    Route::post('/save', [PostController::class, 'save'])->name('post.save');
    Route::delete('/delete/{id}', [PostController::class, 'delete']);
});


// booking
Route::prefix('/booking')->middleware(['auth'])->group(function() {
    // show form utk insert maklumat booking
    Route::get('/form', [BookingController::class, 'create'])->middleware('can:usr');
    Route::get('/form2', [BookingController::class, 'form2']);
    Route::post('/save', [BookingController::class, 'save']);
    Route::put('/save', [BookingController::class, 'store']);
    Route::get('/update/{id}', [BookingController::class, 'update']);
    Route::get('/search', [BookingController::class, 'searchForm']);
    Route::post('/search', [BookingController::class, 'search']);
    Route::get('/edit/{id}', [BookingController::class, 'edit']);
});

// session
Route::prefix('/session')->group(function() {
    // show form utk insert maklumat booking
    Route::get('/', [SessionController::class, 'index']);
    Route::get('/page2', [SessionController::class, 'page2']);
    Route::get('/flush', [SessionController::class, 'flush']);
});


Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

require __DIR__.'/auth.php';
