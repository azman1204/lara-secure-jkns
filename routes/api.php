<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\PostController;

// API for Post
Route::middleware(['auth:sanctum'])->prefix('/post')->group(function() {
    Route::get('/list', [PostController::class, 'list']);
    Route::post('/create', [PostController::class, 'create']);
    Route::put('/update', [PostController::class, 'update']);
});


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware('auth:api')->get('/all-users', function (Request $request) {
    $users = \App\Models\User::all();
    $arr = [];
    foreach ($users as $user) {
        $arr2 = ['name' => $user->name, 'email' => $user->email];
        $arr[] = $arr2;
    }
    return $arr;
});

// global API. semua org boleh access
Route::get('/hello', function() {
    return "Hello World";
});

// Guna Sanctum
Route::post('/tokens/create', function (Request $request) {
    $user = \App\Models\User::find(1);
    $token = $user->createToken($request->token_name);
    return ['token' => $token->plainTextToken];
});

Route::get('/post/all', function() {
    \Log::info("Somebody accessing post request");
    $posts = \App\Models\Post::all();
    return $posts;
})->middleware('auth:sanctum');

// Login guna API
Route::post('login', function(Request $req) {
    $credentials = ['email' => $req->email, 'password' => $req->password, 'status' => 'A'];
    if (\Auth::attempt($credentials)) {
        // jana token
        $user = \Auth::user();
        $token = $user->createToken('spa');
        return ['token' => $token->plainTextToken];
    } else {
        return ['status' => 'Login Failed'];
    }
});