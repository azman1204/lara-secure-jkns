<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

<form action="/booking/search" method="post">
    @csrf
    ID : <br>
    <input type="text" name="id">
    <br>
    <input type="submit" value="Cari">
</form>

@if(isset($rs))
    @foreach($rs as $booking)
        Book Date :{{ $booking->book_dt }}
        <br>
        {{-- Description : {!! $booking->descr !!} --}}
        Description : <?= htmlentities($booking->descr) ?>

        @can('edit', $booking)
            <a href="/booking/edit/{{ $booking->id }}">Edit</>
        @endcan

    @endforeach
    <!-- > &lt; -->
@endif