<form action="/booking/save" method="post">
    @csrf
    @method('put')
    <input type="hidden" name="id" value="{{ $booking->id }}">
    Date : <br>
    <input type="date" name="book_dt" value="{{ $booking->book_dt }}">
    <br>
    Keterangan: <br>
    <input type="text" name="descr" value="{{ $booking->descr }}">
    <br>
    <input type="submit" value="Simpan">
</form>