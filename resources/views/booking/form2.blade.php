<form action="#" method="post" id="myform">
    Date : <br>
    <input type="date" name="book_dt">
    <br>
    Keterangan: <br>
    <input type="text" name="descr">
    <br>
    <input type="submit" value="Simpan">
</form>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script>
$(function() {
    $('#myform').submit(function(e) {
        e.preventDefault();
        var book_dt = $('[name=book_dt]').val();
        var descr = $('[name=descr]').val();
        $.ajax({
            method:'POST',
            url: '/booking/save',
            data: { book_dt:book_dt, descr:descr, _token: '{{ csrf_token() }}' },
            success: function(data) {
                alert('berjaya');
            }
        });
    });
    
});
</script>