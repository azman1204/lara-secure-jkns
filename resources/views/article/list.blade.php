@extends('layouts.master')
@section('content')

<table class="table table-bordered table-stripe table-hover">
    <tr>
        <td>No</td>
        <td>Content</td>
        <td>Action</td>
    </tr>
    @foreach($articles as $article)
    <tr>
        <td>No</td>
        <td>{{ $article->content }}</td>
        <td>
            @can('edit articles')
                <a href="/article/edit/{{ $article->id }}" class="btn btn-primary">Edit</a>
            @endcan
        </td>
    </tr>
    @endforeach
</table>

@endsection