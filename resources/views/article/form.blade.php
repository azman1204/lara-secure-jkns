@extends('layouts.master')
@section('content')

<form action="/article/save" method="post" enctype="multipart/form-data">
    @csrf
    <div class="row">
        <div class="col-md-2">Content</div>
        <div class="col-md-5">
            <input type="text" name="content" class="form-control" placeholder="">
        </div>
    </div>

    <div class="row">
        <div class="col-md-2">Attachment</div>
        <div class="col-md-5">
            <input type="file" name="attachment" class="form-control">
            @error('attachment') {{ $message }} @enderror
        </div>
    </div>

    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-5">
            <input type="submit" class="btn btn-primary" value="simpan">
        </div>
    </div>

</form>

@endsection