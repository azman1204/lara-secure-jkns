<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
<form method="POST" action="/user/registration">
    @csrf

    @if(session()->has('err'))
        <div class="alert alert-danger">{{ session('err') }}</div>
    @endif

    <div class="container">
        <div class="row">
            <div class="col-md-12">Name</div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <input type="text" name="name" class="form-control" placeholder="Name.." value="{{ old('email') }}">
                @error('name') 
                    <span class="text-danger">{{ $message }}</span>
                @enderror
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">User ID</div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <input type="text" name="email" class="form-control" placeholder="email.." value="{{ old('email') }}">
                @error('email') 
                    <span class="text-danger">{{ $message }}</span>
                @enderror
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">Password</div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <input type="password" name="password" class="form-control" placeholder="password..">
                @error('password') 
                    <span class="text-danger">{{ $message }}</span>
                @enderror
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">Password Confirmation</div>
        </div>
        <div class="row">
            <div class="col-md-12"><input type="password" name="password_confirmation" class="form-control" placeholder="password.."></div>
        </div>

        <div class="row">
            <div class="col-md-12"><input type="submit" value="Login" class="btn btn-primary"></div>
        </div>
    </div>
</form>