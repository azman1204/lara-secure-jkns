<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
<form method="POST" action="/user/auth">
    @csrf

    @if(session()->has('err'))
        <div class="alert alert-danger">{{ session('err') }}</div>
    @endif

    <div class="container">
        <div class="row">
            <div class="col-md-12">User ID</div>
        </div>
        <div class="row">
            <div class="col-md-12"><input type="text" name="email" class="form-control" placeholder="email.."></div>
        </div>
        <div class="row">
            <div class="col-md-12">Password</div>
        </div>
        <div class="row">
            <div class="col-md-12"><input type="password" name="password" class="form-control" placeholder="password.."></div>
        </div>
        <div class="row">
            <div class="col-md-12"><input type="submit" value="Login" class="btn btn-primary"></div>
        </div>
    </div>
</form>