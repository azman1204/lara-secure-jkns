@extends('layouts.master')
@section('content')

<table class="table table-striped table-bordered">
    <tr>
        <td>No</td>
        <td>Content</td>
        <td>Action</td>
    </tr>
    @foreach($posts as $post)
        <tr>
            <td>{{ $loop->index++ }}</td>
            <td>{{ $post->content }}</td>
            <td>
                @can('edit', $post) 
                    <a href="/post/edit/{{ $post->id }}" class='btn btn-success'>Edit</a>
                @endcan

                @can('delete', $post)
                    <form method='post' action="/post/delete/{{ $post->id }}" onsubmit="return confirm('Are you sure')">
                        @csrf
                        @method('delete')
                        <input type="submit" value="Delete" class="btn btn-danger">
                    </form>
                @endcan
            </td>
        </tr>
    @endforeach
</table>

@endsection