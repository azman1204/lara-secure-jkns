@extends('layouts.master')
@section('content')

<form action="{{ route('post.save') }}" method="post">
    @csrf
    <input type="hidden" name="id" value="{{ $post->id }}">
    <div class="row">
        <div class="col-md-12">Article Content</div>
    </div>
    <div class="row">
        <div class="col-md-12"><textarea class="form-control" name="content">{{ $post->content }}</textarea></div>
    </div>
    <div class="row">
        <div class="col-md-12"><input type="submit" class="btn btn-primary"></div>
    </div>
</form>
@endsection